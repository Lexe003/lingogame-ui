import {
    LOGIN_ERROR,
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    INIT_LOGIN_REQUEST,
    USER_RESET,
} from "../actions/auth";

export const INITIAL_STATE = {
    isFetching: false,
    isAuthenticated: false,
    email: '',
    accountRoles: [],
    error: '',
};

function authReducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                ...state,
                isAuthenticated: false,
                email: '',
                accountRoles: [],
                error: '',
            };
        case LOGIN_SUCCESS:
            return {
                ...state,
                isFetching: false,
                isAuthenticated: true,
                email: action.payload.email,
                accountRoles: action.payload.accountRoles,
                error: '',
            };
        case LOGIN_ERROR:
            return {
                ...state,
                isFetching: false,
                isAuthenticated: false,
                email: '',
                accountRoles: [],
                error: action.payload.error
            };
        case INIT_LOGIN_REQUEST:
            return {
                ...state,
                isFetching: true,
                isAuthenticated: false,
                email: '',
                accountRoles: [],
            };
        case USER_RESET:
            return {
                ...state,
                isAuthenticated: false,
                email: '',
                accountRoles: [],
            };
        default:
            return state;
    }
}


export default authReducer
import { combineReducers } from 'redux';

import authReducer from './auth';
import { connectRouter } from 'connected-react-router';

const rootReducer = (history) => combineReducers({
    router: connectRouter(history),
    authStore: authReducer,
})

export default rootReducer;

import FetchService from "./fetchservice";
import { ENDPOINTS } from '../constants';

class AuthService extends FetchService {
    login = async (email, password) => {
        const response = await super.fetch(`${ENDPOINTS.auth}`, 'POST',
            {
                "email": email.toLowerCase(),
                "password": password
            })
        if (!response.ok) { throw new Error("Username or password is incorrect") }
        return this.getUser()
    }

    getUser = async () => {
        const response = await super.fetch(`${ENDPOINTS.account}`, 'GET')
        const data = await super.parseJSON(response)
        if (!response.ok) throw new Error(data.message)
        return data;
    }

    logout() {
        return super
            .fetch(`${ENDPOINTS.auth}`, 'DELETE')
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }

    getRoles() {
        return super
            .fetch(`${ENDPOINTS.role}`, 'GET')
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }

    saveAccount(body) {
        return super
            .fetch(`${ENDPOINTS.account}/register`, 'POST', body)
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }
}

export default new AuthService();

import FetchService from './fetchservice';
import { ENDPOINTS } from '../constants';

class ScoreService extends FetchService {
    getScore(gameUuid) {
        return super
            .fetch(`${ENDPOINTS.LINGOGAME}/score/${gameUuid}`, 'GET')
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }

    getScores() {
        return super
            .fetch(`${ENDPOINTS.LINGOGAME}/score`, 'GET')
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }

    saveScore(gameUuid) {
        return super
            .fetch(`${ENDPOINTS.LINGOGAME}/score/${gameUuid}`, 'PATCH')
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }
}

export default new ScoreService();
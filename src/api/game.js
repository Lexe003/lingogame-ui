import FetchService from './fetchservice';
import { ENDPOINTS } from '../constants';

class GameService extends FetchService {
    async triggerProducer() {
        return await fetch(`${ENDPOINTS.LINGOWORDS}/kafka/publish`)
            .then(response => {
                return response.status === 200
            })
    }

    startGame() {
        return super
            .fetch(`${ENDPOINTS.LINGOGAME}/game`, 'GET')
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }

    performAttempt(gameUuid, body) {
        return super
            .fetch(`${ENDPOINTS.LINGOGAME}/game/${gameUuid}/attempt`, 'POST', body)
            .then((data) => super.parseJSON(data))
            .catch((error) => Promise.reject(new Error(error.message)));
    }
}

export default new GameService();
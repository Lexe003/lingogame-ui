import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Route, Switch } from 'react-router-dom';
import NotFound from './components/NotFound/NotFound';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';
import Login from './pages/login/Login';
import Register from './pages/register/Register';
import { useSelector } from 'react-redux';
import GameContainer from './pages/game/GameContainer';
import Header from './components/Header/Header';
import Highscore from './pages/highscore/Highscore';
import { PATH } from './constants';

function App() {
  const { isFetching } = useSelector(state => ({
    isFetching: state.authStore.isFetching
  }))

  if (isFetching) {
    return <div>Loading application...</div>
  }

  return (
    <div className="App d-flex flex-column h-100 w-100">
      <Header />

      <Switch>
        <Route path={PATH.LOGIN} exact component={Login} />
        <Route path={PATH.REGISTER} exact component={Register} />
        <Route path={PATH.NOT_FOUND} exact component={NotFound} />
        <PrivateRoute path={PATH.HIGHSCORE} exact component={Highscore} />
        <PrivateRoute path={PATH.HOME} exact component={GameContainer} />
      </Switch>

    </div>
  );
}

export default App;

import { all } from 'redux-saga/effects';
import { watchLogin, watchInitLogin, watchLogout } from './auth';

export default function* rootSaga() {
    yield all([
        watchLogin(),
        watchInitLogin(),
        watchLogout(),
    ]);
}
import { takeLatest, all, call, put } from 'redux-saga/effects';

import AuthService from '../api/auth'
import {
    LOGIN_REQUEST,
    loginError,
    loginSuccess,
    INIT_LOGIN_REQUEST,
    userReset,
    LOGOUT_REQUEST,
    VALIDATE_ERROR_REQUEST
} from '../actions/auth'

export function* getLoginSaga({ payload }) {
    try {
        const { data, error } = yield call(AuthService.login, payload.email, payload.password);
        if (error) {
            return yield call(resetUserSaga, error);
        }
        let roles = data.accountRoles.map(accountRole =>
            accountRole.role.name
        )
        yield put(loginSuccess(data.email, roles));
        return data;
    } catch (error) {
        yield put(loginError(error.message));
        return error;
    }
}

export function* watchLogin() {
    yield all([takeLatest(LOGIN_REQUEST, getLoginSaga)]);
}

export function* getInitLoginSaga() {
    try {
        const { data, error } = yield call(AuthService.getUser);
        if (error) {
            return yield call(resetUserSaga, error);
        }
        let roles = data.accountRoles.map(accountRole =>
            accountRole.role.name
        )
        yield put(loginSuccess(data.email, roles));
        return data;
    } catch (error) {
        yield put(loginError(error.message));
        return error;
    }
}

export function* watchInitLogin() {
    yield all([takeLatest(INIT_LOGIN_REQUEST, getInitLoginSaga)]);
}

export function* resetUserSaga(error) {
    if (error.status === 401 || error.status === 403) {
        yield put(userReset());
        return error.message;
    }
    throw (error.message);
}

export function* getLogoutsaga() {
    try {
        yield call(AuthService.logout);
        return yield put(userReset());
    } catch (error) {
        yield put(userReset());
        return error;
    }
}

export function* watchLogout() {
    yield all([takeLatest(LOGOUT_REQUEST, getLogoutsaga)]);
}

export function* watchError() {
    yield all([takeLatest(VALIDATE_ERROR_REQUEST, resetUserSaga)]);
}
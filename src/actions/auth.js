
export const INIT_LOGIN_REQUEST = 'App/INIT_LOGIN_REQUEST';
export function initloginRequest() {
    return {
        type: INIT_LOGIN_REQUEST
    };
}

export const LOGIN_REQUEST = 'App/LOGIN_REQUEST';
export function loginRequest(email, password) {
    return {
        type: LOGIN_REQUEST,
        payload: {
            email,
            password,
        }
    };
}

export const LOGIN_SUCCESS = 'App/LOGIN_SUCCESS';
export function loginSuccess(email, accountRoles) {
    return {
        type: LOGIN_SUCCESS,
        payload: {
            email,
            accountRoles
        }
    };
}

export const LOGIN_ERROR = 'App/LOGIN_ERROR';
export function loginError(error) {
    return {
        type: LOGIN_ERROR,
        payload: {
            error
        }
    };
}

export const USER_RESET = 'App/USER_RESET';
export function userReset() {
    return {
        type: USER_RESET
    };
}

export const LOGOUT_REQUEST = 'App/LOGOUT_REQUEST';
export function logoutRequest() {
    return {
        type: LOGOUT_REQUEST
    };
}

export const VALIDATE_ERROR_REQUEST = 'App/VALIDATE_ERROR_REQUEST';
export function validateError(error) {
    return {
        type: VALIDATE_ERROR_REQUEST,
        payload: {
            error
        }
    };
}

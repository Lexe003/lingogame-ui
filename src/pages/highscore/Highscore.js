import React, { useState, useEffect } from 'react'
import ScoreService from '../../api/score'
import DataTable from "react-data-table-component";

export default function Highscore() {
    const [scores, setScores] = useState({ list: [], isFetching: false, error: '' })

    const fetchScores = async () => {
        setScores(prevState => ({ ...prevState, error: '', isFetching: false }))
        try {
            const { data, error } = await ScoreService.getScores();
            if (error) {
                throw new Error(error.message)
            }
            setScores(prevState => ({ ...prevState, list: data, isFetching: false }))
        } catch (error) {
            setScores(prevState => ({ ...prevState, error, isFetching: false }))
        }
    }

    useEffect(() => {
        fetchScores()
    }, [])

    const columns = [
        {
            name: 'NAME',
            selector: 'name',
            sortable: true,
        },
        {
            name: 'SCORE',
            selector: 'amount',
            sortable: true,
        }
    ];

    return (
        <div className="d-flex h-100 m-4">

            <DataTable
                columns={columns}
                defaultSortField='scores'
                data={scores.list}
                pagination
                persistTableHead
                highlightOnHover
                noHeader={true}
                pointerOnHover={true}
            />

        </div>
    )
}

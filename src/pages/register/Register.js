import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import AuthService from '../../api/auth';


export default function Register() {
    const initFormState = { username: '', password: '', isFetching: false, error: '' };
    const [form, setForm] = useState(initFormState)
    const history = useHistory();

    const setFormHandler = (event) => {
        const type = event.target.name
        const value = event.target.value
        setForm(prevState => ({ ...prevState, [type]: value }))
    }

    const submit = async () => {
        const { username, password } = form;
        setForm(prevState => ({ ...prevState, isFetching: true, error: '' }))
        const { data, error } = await AuthService.saveAccount({ username, password });
        if (data) {
            history.push('/login')
        } else {
            setForm(prevState => ({ ...prevState, error: error.message, isFetching: false }))
        }
    }

    return (
        <div className="d-flex justify-content-center align-items-center h-100 w-100">
            <div className="d-flex justify-content-center flex-column align-items-center shadow"
                style={{ height: "300px", width: "400px", padding: "50px", opacity: "85%", borderRadius: "10px", background: "#fff" }}>
                <div style={{ fontWeight: "1000", color: "black" }} className="form-group">SIGNUP</div>
                <div className="d-flex flex-column justify-content-between">
                    <div className="form-group">
                        <input style={{ fontWeight: "1000" }} placeholder="Username" className="form-control" type="text" name="username" value={form.email} onChange={setFormHandler} required />
                    </div>
                    <div className="form-group">
                        <input style={{ fontWeight: "1000" }} placeholder="Password" className="form-control" type="password" value={form.password} name="password" onChange={setFormHandler} required />
                    </div>
                    {form.isFetching ? <div>Loading...</div> : <button type="button" onClick={submit} style={{ fontWeight: "1000" }} className="btn btn-primary">SUBMIT</button>}
                    {form.error && <div className="form-group mt-3">
                        {form.error}
                    </div>}
                </div>
            </div>
        </div>
    )
}

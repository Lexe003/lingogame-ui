import React, { useState, useEffect } from 'react'
import GameHeader from '../GameHeader/GameHeader';
import GameBody from '../GameBody/GameBody';
import GameFooter from '../GameFooter/GameFooter';
import GameService from '../../../api/game';
import ScoreService from '../../../api/score';
import { successAlert } from '../../../utils/alerts';

import './styles.css';

export default function Game({ gameDTO, setGameDTO, _startGame }) {
    const [apiProcess, setApiProcess] = useState({ isFetching: false, error: '' })
    const [game, setGame] = useState({ isFinished: false, round: 0, hasNextRound: false })
    const [score, setScore] = useState(0)
    const [counter, setCounter] = useState(10)
    const [breaks, setBreak] = useState(false)

    useEffect(() => {
        const timer = (counter > 0 && !breaks) && setInterval(() => setCounter(counter - 1), 1200);
        return () => clearInterval(timer);
    }, [counter, breaks]);

    useEffect(() => {
        if (counter === 0 && breaks === false) {
            setBreak(true)
            onExpireHandler()
        }
    }, [counter])

    /* 
        Returns array of objects
        [ { uuid, gameRoundUuid, guessedWord, feedbackOnAttempts: [{ uuid, character, status, feedbackUuid }] } ]
    */
    const getFeedback = _ => {
        const x = getCurrentGameRound().attempts?.sort((a, b) => new Date(a.creationTime) - new Date(b.creationTime))
        return x;
    }

    /*
        Returns object
        { uuid, gameUuid, creationTime, canAttempt, attempt: [ { uuid, gameRoundUuid, guessedWord, feedbackOnAttempts: [{ uuid, character, status, feedbackUuid }] } ] }
    */
    const getCurrentGameRound = _ => {
        const gameRounds = gameDTO.gameRounds.sort((a, b) => new Date(a.creationTime) - new Date(b.creationTime))
        return gameRounds[game.round]
    }

    const hasGuessedWord = () => {
        return Boolean(game.round + 1 < gameDTO.gameRounds.length)
    }

    useEffect(() => {
        if (hasGuessedWord()) {
            setGame(prevState => ({ ...prevState, hasNextRound: true, isFinished: false }))
            setCounter(0)
            setBreak(true)
            _getScore();
        } else {
            if (getCurrentGameRound().canAttempt) {
                setCounter(10);
                setBreak(false);
            } else {
                _getScore()
                setCounter(10);
                setBreak(true);
                setGame(prevState => ({ ...prevState, isFinished: true, hasNextRound: false }))
            }
        }
    }, [gameDTO])

    const onExpireHandler = () => {
        const previousAttempt = getFeedback();
        _performAttempt({ guessedWord: previousAttempt[previousAttempt.length - 1].guessedWord })
    }

    const _performAttempt = async (body) => {
        setApiProcess(prevState => ({ ...prevState, error: '', isFetching: true }))
        setBreak(true)
        try {
            const { data, error } = await GameService.performAttempt(gameDTO.uuid, body);
            if (error) throw new Error(error.message)
            setGameDTO(prevState => ({ ...prevState, data }))
            setApiProcess(prevState => ({ ...prevState, isFetching: false }))
        } catch (e) {
            return setApiProcess(prevState => ({ ...prevState, error: e.message, isFetching: false }))
        }
    }

    const _saveScore = async () => {
        setApiProcess(prevState => ({ ...prevState, error: '', isFetching: true }))
        try {
            const { error } = await ScoreService.saveScore(gameDTO.uuid);
            if (error) throw new Error(error.message)
            setApiProcess(prevState => ({ ...prevState, isFetching: false }))
            successAlert()
        } catch (e) {
            return setApiProcess(prevState => ({ ...prevState, error: e.message, isFetching: false }))
        }
    }

    const _getScore = async _ => {
        setApiProcess(prevState => ({ ...prevState, error: '', isFetching: true }))
        try {
            const { data, error } = await ScoreService.getScore(gameDTO.uuid);
            if (error) throw new Error(error.message)
            setScore(data.amount)
            setApiProcess(prevState => ({ ...prevState, error: '', isFetching: false }))
        } catch (e) {
            return setApiProcess(prevState => ({ ...prevState, error: e.message, isFetching: false }))
        }
    }

    const prepareRoundHandler = () => {
        setGame(prevState => ({ ...prevState, hasNextRound: false, round: prevState.round + 1 }))
        setCounter(10)
        setBreak(false);
    }

    return (<div className="d-flex flex-column justify-content-center align-items-center complete-box" style={{ width: "300px" }}>
        <GameHeader
            roundNumber={game.round + 1}
            score={score}
            time={counter}
        />
        <GameBody
            feedbackList={getFeedback()}
        />
        <GameFooter
            isFetching={apiProcess.isFetching}
            _attempt={_performAttempt}
            _restart={_startGame}
            _saveScore={_saveScore}
            gameState={game}
            onContinue={prepareRoundHandler}
        />
        {apiProcess.error ?? null}
    </div>)
}

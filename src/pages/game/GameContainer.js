import React, { useState } from 'react'
import GameService from '../../api/game';
import Game from './Game/Game';
import Spinner from '../../components/Spinner/Spinner'
import { useDispatch } from 'react-redux';
import { validateError } from '../../actions/auth';

export default function GameContainer() {
    const [gameDTO, setGameDTO] = useState({ isFetching: false, error: '', data: undefined });
    const dispatch = useDispatch()

    const setShowGameHandler = async () => {
        setGameDTO(prevState => ({ ...prevState, error: '', isFetching: true }))
        try {
            const { data, error } = await GameService.startGame();
            if (error) {
                dispatch(validateError(error))
            }
            setGameDTO(prevState => ({ ...prevState, isFetching: false, data }))
        } catch (e) {
            const errorMessage = (e instanceof String) ? e : e.message
            if (e instanceof String) {
                setGameDTO(prevState => ({ ...prevState, error: errorMessage, isFetching: false }))
            }
        }
    }

    if (gameDTO.isFetching) {
        return (
            <div className="d-flex justify-content-center align-items-center h-100 w-100">
                <Spinner />
            </div>
        )
    }

    if (gameDTO.error) {
        return (<div>
            <h5>{gameDTO.error}</h5>
        </div>)
    }

    return (
        <div className="d-flex justify-content-center align-items-center h-100 w-100" >
            {gameDTO.data
                ? <Game
                    gameDTO={gameDTO.data}
                    setGameDTO={setGameDTO}
                    _startGame={setShowGameHandler}
                />
                : <h1 className="text-box"
                    onClick={setShowGameHandler}>
                    Start
                  </h1>
            }
            {gameDTO.error && <div>{gameDTO.error} </div>}
        </div>
    )
}

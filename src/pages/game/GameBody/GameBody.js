import React from 'react'
import Row from './Row'
import './styles.css'

export default function GameBody({ feedbackList }) {

    return (
        <div className="d-flex flex-column gamebody">
            {feedbackList.map((item, index) => {
                return <Row key={index} item={item} />
            })}
        </div>
    )
}

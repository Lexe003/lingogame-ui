import React from 'react'

export default function Row({ item }) {

    const getColor = (status) => {
        switch (status) {
            case 'INVALID':
                return 'red';
            case 'PRESENT':
                return 'yellow';
            case 'CORRECT':
                return 'green';
            case 'ABSENT':
                return 'blue';
            default:
                return 'white';
        }
    }

    const renderCharBox = (character, status, index) => {
        const color = getColor(status);
        return <span
            key={index}
            className="p-2"
            style={{ flex: "1", borderLeft: "1px solid black", backgroundColor: color, padding: "0px !important" }}>
            {character}
        </span>
    }

    return (
        <div className="d-flex justify-content-begin align-items-center">
            {item.feedbackOnAttempts.map((item, index) => {
                return (
                    renderCharBox(item.character, item.status, index)
                )
            })}
        </div>
    )
}

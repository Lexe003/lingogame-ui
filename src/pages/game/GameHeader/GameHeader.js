import React from 'react'
import './styles.css';

export default function GameHeader({ roundNumber, score, time }) {
    return (
        <div className="d-flex justify-content-between align-items-center gameheader"        >
            <span>Round: {roundNumber}</span>
            <span>Score: {score}</span>
            <span>Timer: {time}</span>
        </div>
    )
}

import React, { useState } from 'react'
import './styles.css';

export default function GameFooter({ _attempt, _restart, gameState, onContinue, _saveScore, isFetching }) {
    const { isFinished, round, hasNextRound } = gameState
    const [value, setValue] = useState('');

    const setValueHandler = (input) => {
        const word = input ? input.toLowerCase() : input
        setValue(word)
    }

    const attemptHandler = (e) => {
        e.preventDefault()
        const body = {
            guessedWord: value
        }
        _attempt(body)
        setValue('')
    }

    if (hasNextRound === false && isFinished === true) {
        return <div className="d-flex align-items-center" style={{ width: "300px", padding: "0 2px" }}>
            {round > 0 && <button className="btn btn-success" type='button' onClick={_saveScore} style={{ flex: "1", borderRadius: "0%" }}>SAVE SCORE</button>}
            <button className="btn btn-primary" type='button' onClick={_restart} style={{ flex: "1", borderRadius: "0%" }}>NEW GAME</button>
        </div>
    }

    const numChars = 5 + round

    return (
        <div className="d-flex align-items-center gamefooter">
            {hasNextRound
                ?
                <button
                    className="btn btn-primary"
                    onClick={onContinue}
                    style={{ flex: "1", borderRadius: "0%" }}
                >
                    NEXT ROUND =>
                </button>
                :
                <form className="d-flex w-100" onSubmit={attemptHandler}>
                    <input
                        pattern={`^[a-z]{${numChars}}$`}
                        title={`Must containt ${numChars} characters`}
                        style={{ flex: "1" }}
                        type="text"
                        placeholder="Fill in..."
                        value={value}
                        onChange={(e) => setValueHandler(e.target.value)}
                        required
                    />
                    <button className="btn btn-primary" style={{ flex: "1", borderRadius: "0%" }} type="submit" value="Attempt" disabled={isFetching}>
                        {isFetching && <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>}
                            Attempt
                        </button>
                </form>
            }
        </div>
    )
}

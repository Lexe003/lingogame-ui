import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { loginRequest } from '../../actions/auth'

export default function Login(props) {
    const { from } = props.location.state || { from: { pathname: '/' } }
    const initFormState = { email: '', password: '', isFetching: false };
    const [form, setForm] = useState(initFormState)
    const dispatch = useDispatch();

    const { isAuthenticated, error, isFetching } = useSelector(state => ({
        isAuthenticated: state.authStore.isAuthenticated,
        error: state.authStore.error,
        isFetching: state.authStore.isFetching
    }))

    if (isAuthenticated) {
        return <Redirect to={from} />
    }

    const login = () => {
        const { email, password } = form;
        return dispatch(loginRequest(email, password))
    }

    return (
        <div className="d-flex justify-content-center align-items-center h-100 w-100">

            <div className="d-flex justify-content-center flex-column align-items-center shadow"
                style={{ height: "300px", width: "400px", padding: "50px", opacity: "85%", borderRadius: "10px", background: "#fff" }}>
                <div style={{ fontWeight: "1000", color: "black" }} className="form-group">Login</div>
                <form className="d-flex flex-column justify-content-between">
                    <div className="form-group">
                        <input style={{ fontWeight: "1000" }} placeholder="Username" className="form-control" type="text" value={form.email} onChange={({ target }) => setForm(prevState => ({ ...prevState, email: target.value }))} required />
                    </div>
                    <div className="form-group">
                        <input style={{ fontWeight: "1000" }} placeholder="Password" className="form-control" type="password" value={form.password} onChange={({ target }) => setForm(prevState => ({ ...prevState, password: target.value }))} required />
                    </div>
                    {isFetching ? <div>Loading...</div> : <button style={{ fontWeight: "1000" }} type="button" onClick={login} className="btn btn-primary">Login</button>}

                    {error &&
                        <div className="form-group mt-3 d-flex justify-content-center align-items-center c-red">
                            {error}
                        </div>
                    }
                </form>
            </div>
        </div>

    )
}

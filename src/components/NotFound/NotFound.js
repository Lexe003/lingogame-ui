import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () =>
    <div style={{ display: "flex", height: "100%", justifyContent: "center", alignItems: "center" }}>
        <div style={{ display: 'grid' }}>
            <h1>404 - NOT FOUND</h1>
            <Link
                className="btn btn--secondary"
                style={{ margin: '30px auto 0' }}
                to={'/'}>
                <span className="t-5">HOME</span>
            </Link>
        </div>
    </div>;

export default NotFound;

import React, { Component } from 'react'
import { Redirect, Route } from 'react-router-dom'
import { connect } from 'react-redux';

export class PrivateRoute extends Component {
    render() {
        const { isAuthenticated, component, history, ...rest } = this.props;

        const renderComponent = (props) => {
            if (isAuthenticated) {
                const ComponentToShow = component;
                return <ComponentToShow {...props} />
            } else {
                return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
            }
        };

        return (
            <Route
                {...rest}
                render={renderComponent}
            />
        )
    }
}

export default connect(
    (state) => ({
        isAuthenticated: state.authStore.isAuthenticated
    })
)(PrivateRoute);
import React from 'react'
import { PATH } from '../../constants'
import { NavLink } from 'react-router-dom'
import './styles.css'
import { useDispatch, useSelector } from 'react-redux'
import { logoutRequest } from '../../actions/auth'

export default function Header() {
    const { isAuthenticated } = useSelector(state => ({
        isAuthenticated: state.authStore.isAuthenticated
    }))

    const dispatch = useDispatch()

    return (
        <div className="d-flex justify-content-center align-items-center w-100 p-4 header">
            <ul className="d-flex p-0 m-0" style={{ listStyle: "none", flexWrap: "wrap" }}>
                {isAuthenticated ?
                    <>
                        <li className="mr-2">
                            <NavLink activeClassName="active" to={PATH.HOME}>HOME</NavLink>
                        </li>
                        <li>
                            <NavLink activeClassName="active" to={PATH.HIGHSCORE}>HIGHSCORE</NavLink>
                        </li>
                        <li>
                            <span href={"#"} style={{ cursor: "pointer" }} onClick={() => dispatch(logoutRequest())}>LOGOUT</span>
                        </li>
                    </>
                    :
                    <>
                        <li>
                            <NavLink to={PATH.LOGIN}>LOGIN</NavLink>
                        </li>
                        <li>
                            <NavLink to={PATH.REGISTER}>REGISTER</NavLink>
                        </li>
                    </>
                }
            </ul>
        </div>
    )
}

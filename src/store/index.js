import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from '../reducers/index';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../sagas';
import { createBrowserHistory } from 'history'

export const history = createBrowserHistory()

const configureStore = () => {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        rootReducer(history),
        compose(
            applyMiddleware(sagaMiddleware),
            (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()) || compose
        ));
    sagaMiddleware.run(rootSaga);
    return store;
}

export default configureStore;
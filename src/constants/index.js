export const ENDPOINTS = {
    auth: `${process.env.REACT_APP_LINGOGAME_API}/token`,
    account: `${process.env.REACT_APP_LINGOGAME_API}/account`,
    LINGOWORDS: `${process.env.REACT_APP_LINGOWORD_API}`,
    LINGOGAME: `${process.env.REACT_APP_LINGOGAME_API}`
}


export const PATH = {
    HOME: `/`,
    LOGIN: '/login',
    REGISTER: '/register',
    HIGHSCORE: '/highscore',
    NOT_FOUND: '/notfound'
}